# Clothes-widget-3d

## Setup

Add dependency to the `package.json` file.

```json
  "dependencies": {
    ...
    "clothes-widget-3d": "git+ssh://git@bitbucket.org:haha29/widget3d.git#master"
    ...
  }
```

Then install it.

```bash
npm i
```


## Usage

```js
import Widget3D from 'clothes-widget-3d';

const widget3d = new Widget3D(document.getElementById('app'), {
  // API URL
  apiPath: 'http://194.87.239.90:8081/api/',
  // Path where widget3d assets located
  assetsPath: 'http://194.87.239.90/assets/models/',
  // Salon ID (need for API)
  salonId: 1,
  // Set device pixelRatio specific value (by default it equals to `window.devicePixelRatio`)
  pixelRatio: 1,
  // Force using low quality textures for mobile devices (by default enabled for mobiles and tablets)
  useMobileTextures: true,
  // Force using camera positions for mobile devices (by default disabled)
  useMobilePositions: true,
  // Asset click handler
  onClickAsset({ id }) {
        console.log('asset was clicked:', id);
        // To select clicked asset
        widget3d.select({ id });
    }
  },
  // Webgl context lost event
  onLostContext() {
    console.log('webgl context lost');
  }
);

// To update assets list
widget3d.update([
    'frt1',
    'bck1',
    'yke1',
    'fab1',
    'btn1',

    // Load object asset but don't show on scene
    { id: 'slv1', hidden: true },

    // Load object asset and apply specified materials only to this material
    { id: 'clr1', materials: ['fab1'] },

    // Generated text
    { id: 'irt9', text: { value: 'Hello', font: 'ist1', color: 'inc1' } },

    // Use static property for non interactive assets
    { id: 'head', static: true },
    { id: 'body', static: true },
    { id: 'trousers', static: true }
]).then(() => console.log('assets updated'));

// Resolves after first update
widget3d.firstUpdate.then(() => {
    console.log('first update');
});

// To select asset
widget3d.select('yke1');

// To set canvas size
window.addEventListener('resize', function() {
    widget3d.setCanvasSize(); // take width and height of canvas's parent
    // or specify canvas size
    // widget3d.setCanvasSize(window.innerWidth, window.innerHeight);
});

// Dispose application (clear links, stop processes, remove event handlers)
widget3d.dispose();
```


### For React users

```js
// Widget.js
import React, { PureComponent } from 'react';
import './Widget.css';
import Widget3D from 'clothes-widget-3d';

export default class WidgetComponent extends PureComponent {
  componentDidMount() {
    this.widget3d = new Widget3D(this.widgetContainer, {
      apiPath: 'http://194.87.239.90:8081/api/',
      assetsPath: 'http://194.87.239.90/assets/models/',
      salonId: 1,
      onClickAsset({ id }) {
        this.props.onClickAsset(id);
    });
}
    });
    this.widget3d.update(this.props.assets).then(this.handleUpdated);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.assets !== this.props.assets) {
      this.widget3d.update(this.props.assets).then(this.handleUpdated);
    }

    if (prevProps.selected !== this.props.selected) {
      this.widget3d.select(this.props.selected);
    }
  }

  componentWillUnmount() {
    this.widget3d.dispose();
  }

  render() {
    return <div className="widget3d" ref={(node) => this.widgetContainer = node} />;
  }

  handleUpdated = () => {
    console.log('updated');
  }
}
```

```js
// App.js
import React, { Component } from 'react';
import Widget from './Widget';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Widget
          selected='slv1'
          assets={[
            'frt1',
            'bck1',
            'clr1',
            'slv1',
            'yke1',
            'fab1',
            'btn1',
            { 'id': 'head', 'static': true },
            { 'id': 'body', 'static': true },
            { 'id': 'trousers', 'static': true }
          ]}
          onClickAsset={this.handleClickAsset}
        />
      </div>
    );
  }

  handleClickAsset = ({ id }) => {
    console.log('asset was clicked:', id);
  };
}
```