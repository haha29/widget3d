/* global clothesWidget3d */
/* eslint-disable */

var element = document.getElementById('app');
var Widget3d = clothesWidget3d.default;

var widget3d;

function init() {
    widget3d = new Widget3d(element, {
        apiPath: 'http://194.87.239.90:8081/api/',
        assetsPath: 'http://194.87.239.90/assets/models/',
        salonId: 1,
        // pixelRatio: 1,
        // useMobileTextures: true,
        // useMobilePositions: true,
        onClickAsset: function(data) {
            widget3d.select(data.id);
        },
        onLostContext: function() {
            console.log('webgl context lost');
        }
    });

    widget3d.firstUpdate.then(function() {
        console.log('first update');
    });

    window.addEventListener('resize', function() {
        widget3d.setCanvasSize();
    });
}

init();

fetch('scripts/view.json')
    .then(function(res) { return res.json() })
    .then(function(assets) {
        widget3d.update(assets);

        window.addEventListener('keypress', function(event) {
            if (event.key === 'q') {
                element.style.opacity = 0.4;
                widget3d.update([
                    "frt2",
                    "bck1",
                    { "id": "clr1", "materials": ["fab1"] },
                    "slv1",
                    "yke1",
                    "cfs1",
                    "btc1",
                    "pkt1",

                    "fab2",
                    "btn1",
                    "smc1",

                    { "id": "irt8", "text": { "value": "421", "font": "ist2", "color": "inc2" } },

                    { "id": "eyes", "static": true },
                    { "id": "head", "static": true },
                    { "id": "body", "static": true },
                    { "id": "trousers", "static": true },
                    { "id": "shoes", "static": true }
                ]).then(function () {
                    element.style.opacity = 1;
                });
            }

            if (event.key === 'd') {
                widget3d.dispose();
            }

            if (event.key === 'i' && widget3d.disposed) {
                init();
                widget3d.update(assets);
            }
        });
    });