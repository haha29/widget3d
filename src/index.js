import Viewer from './viewer/app';
import Assets from './assets';
import createStore from './store';
import * as Actions from './actions';
import pkg from '../package.json';
import dispose from './utils/dispose';

const prefix = '[clothes-widget-3d]';

function log(...attrs) {
    console.log(prefix, ...attrs);
}

function logError(...attrs) {
    console.error(prefix, ...attrs);
}

if (typeof pkg._resolved === 'string') {
    const [, hash] = pkg._resolved.split('#');
    log('version', hash);
}

export default class Widget3D {
    constructor(element, {apiPath, assetsPath, dracoPath, salonId, useMobileTextures, useMobilePositions, pixelRatio, dummyAssetsId, onClickAsset, onRotate, onLostContext} = {}) {
        const store = createStore();
        const assets = new Assets({config: {apiPath, assetsPath, dracoPath, salonId, garmentIds: dummyAssetsId}});
        const viewer = new Viewer({assets, store, onClickAsset, onRotate, pixelRatio, useMobileTextures, useMobilePositions});

        viewer.gl.renderer.context.canvas.addEventListener('webglcontextlost', this._handleContextLost);

        element.appendChild(viewer.domElement);

        this.assets = assets;
        this.viewer = viewer;
        this.store = store;
        this.element = element;
        this.onClickAsset = onClickAsset;
        this.onLostContext = onLostContext;
        this._loadedAssets = [];
        this.disposed = false;

        window._clothesWidget3d = this;

        this.setCanvasSize();

        this._isfirstUpdateResolved = false;
        this._firstUpdateResolve = () => {};
        this.firstUpdate = new Promise((resolve) => {
            this._firstUpdateResolve = resolve;
        }).then(() => {
            this._isfirstUpdateResolved = true;
        });
    }

    setCanvasSize(w, h) {
        if (this.disposed) {
            logError('failed to setCanvasSize: application disposed');
            return;
        }

        w = w || this.element.clientWidth;
        h = h || this.element.clientHeight;

        log('setCanvasSize', `${w}x${h}`);
        this.store.dispatch(Actions.setCanvasSize(w, h));
    }

    select(data) {
        if (this.disposed) {
            logError('failed to select: application disposed');
            return;
        }

        log('select', JSON.stringify(data));
        return this.viewer.select(data);
    }

    update(data) {
        if (this.disposed) {
            logError('failed to update: application disposed');
            return;
        }

        this._loadedAssets = data;
        const startTime = Date.now();

        return this.viewer.update(data).then(() => {
            const delta = Date.now() - startTime;
            log('update', `${delta}ms`, JSON.stringify(data));

            if (!this._isfirstUpdateResolved) {
                this._firstUpdateResolve();
            }
        });
    }

    dispose() {
        if (this.viewer) {
            this.element.removeChild(this.viewer.domElement);
            this.viewer.dispose();
        }
        dispose(this);
        this.disposed = true;
    }

    _handleContextLost = (event) => {
        event.preventDefault();
        logError('webgl context lost');

        if (this.viewer) {
            this.element.removeChild(this.viewer.domElement);
            this.viewer.dispose();
        }

        if (this.onLostContext) {
            this.onLostContext();
        }

        setTimeout(this._restore, 1000);
    }

    _restore = () => {
        let viewer;
        try {
            viewer = new Viewer({
                assets: this.assets,
                store: this.store,
                onClickAsset: this.onClickAsset
            });
        } catch (err) {
            logError(err);
            logError('failed to restore context, retrying...');
            setTimeout(this._restore, 2000);
            return;
        }

        viewer.gl.renderer.context.canvas.addEventListener('webglcontextlost', this._handleContextLost);

        this.element.appendChild(viewer.domElement);
        this.viewer = viewer;

        this.setCanvasSize();
        this.update(this._loadedAssets);
    }
}
