export default function(object) {
    Object.keys(object).forEach((key) => {
        object[key] = null;
    });
}
