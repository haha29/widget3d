import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

let defToolsExtensionMiddleware = f => f;
if (process.env.NODE_ENV === 'development' && window.devToolsExtension) {
    defToolsExtensionMiddleware = window.devToolsExtension();
}

export default () => {
    return createStore(reducers, compose(
        applyMiddleware(thunk),
        defToolsExtensionMiddleware
    ));
};
