import * as Config from '../action-types';
import isWebglSupported from '../utils/is-webgl-supported';

const initialState = {
    canvasSize: [0, 0],
    isModelLoaded: false,
    isWebglSupported,
    isDebug: ['?debug', '?gui'].includes(window.location.search)
};

export default function(state = initialState, action) {
    switch (action.type) {
    case Config.SET_CANVAS_SIZE:
        return {
            ...state,
            canvasSize: [action.width, action.height]
        };

    case Config.LOAD_MODEL: {
        return {
            ...state,
            isModelLoaded: true
        };
    }

    default:
        return state;
    }
}
