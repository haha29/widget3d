import pick from 'lodash/pick';
import {bindActionCreators} from 'redux';
import * as THREE from 'three';

import dispose from '../../utils/dispose';
import * as Actions from '../../actions';

import config from './config';

import Gl from '../gl';
import Env from '../env';
import Views from '../views';
import Controls from '../controls';
import Raycastser from '../raycaster';
import Material from '../material';
import Tweening from '../tweening';

import AnimationCtrl from './animationCtrl';

export default class Viewer {
    constructor({assets, store, pixelRatio, useMobileTextures, useMobilePositions, onClickAsset = () => {}, onRotate = () => {}}) {
        const state = store.getState();
        const actions = bindActionCreators(Actions, store.dispatch);
        if (!state.isWebglSupported) { return; }

        this.actions = actions;
        this.config = config;
        this.assets = assets;
        this.store = store;
        this.state = state;
        this.handlers = {onClickAsset};

        this.env = new Env({config});

        this.tweening = new Tweening();

        this.views = new Views({
            config,
            assets,
            useMobileTextures,
            onLoad: (...args) => {
                if (state.isDebug) {
                    this.debugReady.then(() => {
                        this._handleLoad(...args);
                    });
                    return;
                }

                this._handleLoad(...args);
            },
            env: this.env
        });
        this.env.wrapper.add(this.views.container);
        // this.views.container.rotation.fromArray([-0.20538257057366682, -2.7460410361886547, 0]);

        if (pixelRatio) {
            Object.assign(config.renderer, {pixelRatio});
        }

        this.gl = new Gl({config: config.renderer, scene: this.env.scene});

        this.controls = new Controls({
            onRotate,
            object: this.views.container,
            config: pick(config, ['camera', 'controls']),
            domElement: this.gl.domElement
        });
        this.gl.camera = this.controls.camera;
        this.env.setCamera(this.controls.camera);

        const raycaster = new Raycastser(this.gl.camera, this.gl.renderer.domElement);
        this.raycaster = raycaster;

        this.material = new Material({
            anisotropy: Math.min(4, this.gl.renderer.capabilities.getMaxAnisotropy()),
            isDebug: state.isDebug,
            onChange: () => {
                this.gl.runOnce();
            }
        });

        // Object.assign(this.controls, {
        //     onStartUpdate: raycaster.disable,
        //     onFinishUpdate: raycaster.enable,
        //     onUpdate: this._updatePopupPosition.bind(this)
        // });

        this.domElement = this.gl.domElement;

        this.gl.setMainLoop(() => {
            if (this.views.container.children.length) {
                this.gl.render();
            }
        });
        this.gl.setConstLoop(() => {
            this.tweening.animate();
            this.controls.animate();
        });
        this.gl.runConstLoop();

        if (state.isDebug) {
            window.THREE = THREE;

            this.debugReady = new Promise((resolve) => {
                /* eslint-disable import/no-unresolved, global-require */
                require.ensure([], () => {
                    const Stats = require('../stats').default;
                    try {
                        this.gui = require('../debug-gui').create(this.gl.domElement, this.controls.camera, this.env.scene);
                    } catch (_) {
                        this.gui = {
                            update: () => {}
                        };
                    }
                    this.gui.update();

                    this.stats = new Stats({renderer: this.gl.renderer});
                    this.gl.addAdditionalLoop(this.stats.animate);

                    resolve();
                }, 'debug');
                /* eslint-enable */
            });
        }

        this._size = [0, 0];

        this.animationCtrl = new AnimationCtrl({app: this, useMobilePositions});

        this._addListeners();
    }

    dispose() {
        this.controls.dispose();
        this.env.dispose();
        this.gl.dispose();
        this.material.dispose();
        this.raycaster.dispose();
        this.tweening.dispose();
        this.views.dispose();
        if (typeof this.storeUnsubscribe === 'function') {
            this.storeUnsubscribe();
        }
        dispose(this);
    }

    select(garment) {
        const data = this.views.getNormalizedGarment(garment);

        // Emissive object blinking selection
        if (data) {
            const meshes = this.views.getMeshes(data);

            this.animationCtrl.start('selection');
            Promise.all(
                meshes.map((mesh) => {
                    const emissive = mesh.material.uniforms ?
                        mesh.material.uniforms.emissive.value :
                        mesh.material.emissive;

                    return Promise.resolve()
                        .then(() => this.tweening.run(emissive, {r: 0, g: 0.07, b: 0.07}, 400))
                        .then(() => this.tweening.run(emissive, {r: 0, g: 0, b: 0}, 400));
                })
            ).then(() => {
                this.animationCtrl.stop('selection');
            });
        }

        this.animationCtrl.moveToGarment(data);
    }

    update(view) {
        return this.views.show(view);
    }

    _addListeners() {
        if (!this.state.isDebug) {
            this.controls.onStartUpdate = () => {
                this.animationCtrl.start('controls');
            };

            this.controls.onFinishUpdate = () => {
                this.animationCtrl.stop('controls');
            };
        }

        this.storeUnsubscribe = this.store.subscribe(() => {
            const state = this.store.getState();

            this._updateCanvasSize(...state.canvasSize);

            if (!state.isModelLoaded) { return; }

            // console.log('state', state);

            this.gl.runOnce();

            this.state = state;
        });
    }

    _handleLoad(view, {meshes, assets, materials}) {
        const RAYCASTER_TAG = 'RACASTING';

        this.raycaster.addSetup(RAYCASTER_TAG, {
            objects: meshes,
            onHover: (mesh) => {
                if (mesh.userData.asset.static) {
                    this.domElement.style.cursor = 'default';
                    return;
                }

                this.domElement.style.cursor = 'pointer';
            },
            onUnhover: () => {
                this.domElement.style.cursor = 'default';
            },
            onClick: (mesh) => {
                if (mesh.userData.asset.static) { return; }

                const {id} = mesh.userData.asset;
                this.handlers.onClickAsset({id});
            }
        });
        this.raycaster.enableSetup(RAYCASTER_TAG);

        assets.forEach((modelAssets) => {
            if (!modelAssets.object3d) { return; }

            if (modelAssets.text) {
                modelAssets.text.path = this.assets.config.assetsPath;
            }

            try {
                this.material.setModel(modelAssets);
            } catch (error) {
                const {id} = modelAssets;
                console.error(error);
                console.error(`[clothes-widget-3d] Failed to apply garment's material ${JSON.stringify({id})}`);
            }
        });

        materials.forEach((materialAssets) => {
            this.material.setMaterial(materialAssets);
        });

        if (this.state.isDebug) {
            this.gl.run();
            this.gui.update();
        }

        this.actions.loadModel();
    }

    _updateCanvasSize(width, height) {
        if (width === this.state.canvasSize.width &&
            height === this.state.canvasSize.height) {
            return;
        }

        this.gl.updateSize(width, height);
        this.controls.updateRatio(width, height);

        this._size[0] = width;
        this._size[1] = height;
    }

    _get2dCoords(object3d, {offsetY = 0} = {}) {
        object3d.getWorldPosition((this._screenVector));
        this._screenVector.y += offsetY;
        this._screenVector.project(this.controls.camera);

        return [
            Math.round(((this._screenVector.x + 1) * this._size[0]) / 2),
            Math.round(((1 - this._screenVector.y) * this._size[1]) / 2)
        ];
    }
}
