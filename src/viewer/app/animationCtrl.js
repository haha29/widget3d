import * as THREE from 'three';

const NORMAL_DISTANCE = 50;

function getTypeById(id) {
    const match = id.match(/^(.*?)\d+$/);
    return match ? match[1] : id;
}

export default class AnimationCtrl {
    constructor({app, useMobilePositions}) {
        Object.assign(this, {
            app,
            animations: new Set()
        });

        this.settings = {};
        this.loadPositions({useMobilePositions});

        if (app.state.isDebug) {
            this.addDebugCameraPositionLogger();
        }
    }

    async loadPositions({useMobilePositions}) {
        const {assets} = this.app;

        if (useMobilePositions) {
            try {
                this.settings = await assets.getById('garment', 'positions_mob');
                this.setInitPosition(this.settings);
                return;
            } catch (e) {
                console.error('Failed to load mobile positions. Fallback to desktop positions.');
            }
        }

        try {
            this.settings = await assets.getById('garment', 'positions');
            this.setInitPosition(this.settings);
        } catch (e) {
            console.error('Failed to load positions.');
        }
    }

    setInitPosition(json) {
        const {controls} = this.app;

        // Set init
        if (!json._init) { return; }

        const {camera, ...controlsConfig} = json._init;
        controls.setConfig(controlsConfig);

        if (controls.target) {
            controls._controls.targetInit.fromArray(controlsConfig.target);
        }

        if (Array.isArray(camera)) {
            controls.camera.position.fromArray(camera);
        }
    }

    addDebugCameraPositionLogger() {
        const {app} = this;

        const button = document.createElement('button');
        button.innerHTML = 'Log camera position';
        button.style.position = 'absolute';
        button.style.left = '90px';
        button.style.top = '10px';
        button.addEventListener('click', () => {
            const json = JSON.stringify({
                target: app.controls.target.toArray(),
                camera: app.controls.camera.position.toArray()
            }, null, 4);

            console.log(json);
        });

        document.body.appendChild(button);
    }

    animate(targetArray, cameraArray) {
        const app = this.app;
        const controls = app.controls._controls;

        const target = new THREE.Vector3().fromArray(targetArray);
        const camera = new THREE.Vector3().fromArray(cameraArray);

        const distance = controls.object.position.distanceTo(camera);
        const animationTime = Math.max(300, 400 * (distance / NORMAL_DISTANCE));

        app.tweening.stopTween();

        return Promise.all([
            app.tweening.run(controls.object.position, camera, animationTime),
            app.tweening.run(controls.target, target, animationTime)
        ]).then(() => app.raycaster.enable());
    }

    moveToGarment(data) {
        if (!data) { return; }

        const type = getTypeById(data.id);
        const settings = this.settings[data.id] || this.settings[type] || this.settings._default;

        if (!settings) { return; }

        this.animate(settings.target, settings.camera);
    }

    start(type) {
        this.animations.add(type);
        this.app.gl.run();
    }

    stop(type) {
        this.animations.delete(type);

        if (!this.animations.size && !this.app.state.isDebug) {
            this.app.gl.stop();
        }
    }
}
