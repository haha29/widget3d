const devicePixelRatio = window.devicePixelRatio || 1;
const isSafariBrowser = navigator.userAgent.includes('Safari') && !navigator.userAgent.includes('Chrome');

const angleDeviation = 30;

export default {
    renderer: {
        clearColor: 0x000000,
        alpha: true,
        antialias: devicePixelRatio === 1 || !isSafariBrowser
    },

    camera: {
        position: [0, 47, 300]
    },

    controls: {
        screenSpacePanning: true,
        minDistance: 50,
        maxDistance: 300,
        minYPan: -80,
        maxYPan: 80,
        maxXZPan: 55,
        // zoomSpeed: 100,
        // panSpeed: 0.5,
        // rotateSpeed: 0.25,
        // enableDamping: true,
        minPolarAngle: ((90 - angleDeviation) * Math.PI) / 180,
        maxPolarAngle: ((90 + angleDeviation) * Math.PI) / 180,
        // enableKeys: false,
        target: [0, 0, 0]
    }
};
