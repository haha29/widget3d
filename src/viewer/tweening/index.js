/* eslint-disable class-methods-use-this */

import TWEEN from 'tween.js';
import forEach from 'lodash/forEach';

import dispose from '../../utils/dispose';

export default class Tweening {
    constructor() {
        Object.assign(this, {
            _tweensNum: 0,
            _tweensByName: {},
            easing: TWEEN.Easing,
            interpolation: TWEEN.Interpolation
        });
    }

    dispose() {
        this.stopAll();
        dispose(this);
    }

    isRunning() {
        return !!this._tweensNum.length;
    }

    stopTween(name) {
        const tween = this._tweensByName[name];

        if (!tween) { return; }

        tween.stop();
    }

    stopAll() {
        TWEEN.removeAll();
    }

    run(obj, to, time, options = {}) {
        return new Promise((resolve) => {
            const name = options.name || 'noname';

            const tween = new TWEEN.Tween(obj).to(to, time);

            const onDone = () => {
                this._tweensNum -= 1;

                this._tweensByName[name] = null;

                resolve();
            };

            tween.onComplete(onDone).onStop(onDone);

            this._tweensByName[name] = tween;

            forEach(options, (option, optName) => {
                if (typeof tween[optName] !== 'function') { return; }

                tween[optName](option);
            });

            tween.start();
            this._tweensNum += 1;
        });
    }

    animate() {
        TWEEN.update();
    }
}
