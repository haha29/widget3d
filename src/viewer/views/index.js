import * as THREE from 'three';
import cloneDeep from 'lodash/cloneDeep';
import {checkMobileOrTablet, createSquareCanvas} from '../utils';

import dispose from '../../utils/dispose';

export default class Views {
    constructor({config, assets, onLoad, env, useMobileTextures = checkMobileOrTablet(), container = new THREE.Object3D()}) {
        container.name = container.name || 'Models';

        Object.assign(this, {
            config,
            assets,
            onLoad,
            env,
            container,
            currentView: null,
            data: {}
        });

        this._useMobileTextures = useMobileTextures;
        this._loadedAssets = {};
    }

    dispose() {
        this.assets.actualizeCache('model', []);
        this.assets.actualizeCache('texture', []);
        this.assets.actualizeCache('garment', []);
        dispose(this);
    }

    async getAsset(type, id) {
        this._loadedAssets[type] = this._loadedAssets[type] || [];
        this._loadedAssets[type].push(id);

        return await this.assets.getById(type, id);
    }

    async loadGarmentAssets(garmentToLoad) {
        garmentToLoad = this.getNormalizedGarment(garmentToLoad); // eslint-disable-line no-param-reassign

        let garmentData;

        try {
            if (garmentToLoad.text) {
                if (garmentToLoad.text.font) {
                    const fontSettings = await this.getAsset('garment', garmentToLoad.text.font);
                    delete garmentToLoad.text.font;

                    Object.assign(garmentToLoad.text, fontSettings);
                }

                if (garmentToLoad.text.color) {
                    const colorSettings = await this.getAsset('garment', garmentToLoad.text.color);
                    delete garmentToLoad.text.color;

                    if (colorSettings.settings && colorSettings.settings.materials && colorSettings.settings.materials.fiber && colorSettings.settings.materials.fiber.diffuse) {
                        garmentToLoad.text.color = colorSettings.settings.materials.fiber.diffuse;
                    } else {
                        Object.assign(garmentToLoad.text, colorSettings);
                    }
                }
            }

            if (garmentToLoad.materials) {
                garmentToLoad.materials = await Promise.all(
                    garmentToLoad.materials.map((material) => {
                        return this.loadGarmentAssets(material);
                    })
                );

                garmentToLoad.materials.filter(Boolean).forEach((material) => {
                    material.objectApply = garmentToLoad.id;
                });
            }

            garmentData = await this.getAsset('garment', garmentToLoad.id);
        } catch (error) {
            console.error(`[clothes-widget-3d] Failed to load garment ${JSON.stringify(garmentToLoad)}`);
            return null;
        }

        const texturesByName = {};

        const assets = {
            ...garmentToLoad,
            ...garmentData,
            object3d: null,
            textures: texturesByName
        };

        const getMobileUrl = (textureUrl) => {
            const [folder, ...otherParts] = textureUrl.split('/');
            return [folder + '_mob', ...otherParts].join('/');
        };

        let garmentTextures = garmentData.textures;
        if(!garmentTextures && garmentToLoad.text) {
            garmentTextures = garmentToLoad.text.textures; assets.text.textures = texturesByName;
        }

        garmentTextures = garmentTextures || {};

        const getTextureUrl = (textureName) => {
            const textureUrl = garmentTextures[textureName];

            if (!this._useMobileTextures) {
                return textureUrl;
            }

            if (textureUrl && typeof textureUrl === 'object') {
                return {
                    ...textureUrl,
                    src: getMobileUrl(textureUrl.src)
                };
            }

            return getMobileUrl(textureUrl);
        };

        const loadingTextures = Object.keys(garmentTextures).map(async (textureName) => {
            const textureUrl = getTextureUrl(textureName);
            const texture = await this.getAsset('texture', textureUrl);

            texturesByName[textureName] = texture;
            return texture;
        });

        let result;
        try {
            result = await Promise.all([
                garmentData.model ? this.getAsset('model', garmentData.model) : Promise.resolve(),
                Promise.all(loadingTextures)
            ]);
        } catch (error) {
            console.error('[clothes-widget-3d] Failed to load garment\'s resources ' + JSON.stringify(garmentToLoad));
            return null;
        }
        const [object3d] = result;

        if (object3d) {
            assets.object3d = object3d;
            delete assets.model;
        }

        // load extra maps if any

        await this._loadExtraMaps(assets, 'alphaMap');
        await this._loadExtraMaps(assets, 'aoSecondMap');

        return assets;
    }

    async _loadExtraMaps(assets, extraTextureSet) {
        // assets пиджаков содержат в себе наборы текстур alphaMap и aoSecondMap, предназначенные для других мешей
        if ((assets[extraTextureSet] instanceof Array) && (assets[extraTextureSet].length > 0) && assets[extraTextureSet][0].url) {
            const maps = await Promise.all(
                assets[extraTextureSet].map( async (map) => {
                    return await this.getAsset('texture', map.url);
                })
            );
            assets[extraTextureSet].forEach( (map, index) => {
                delete map.url; map.texture = maps[index];
            });
        }
    }

    async show(_garmentsToLoad) {
        const garmentsToLoad = cloneDeep(_garmentsToLoad);
        this.meshes = [];

        this._loadedAssets = {};
        const assetsAll = await Promise.all(
            garmentsToLoad.map(garmentToLoad => this.loadGarmentAssets(garmentToLoad))
        );
        const assets = assetsAll.filter(Boolean);

        if (this._loadedAssets.model) {
            this.assets.actualizeCache('model', this._loadedAssets.model);
        }
        if (this._loadedAssets.texture) {
            this.assets.actualizeCache('texture', this._loadedAssets.texture);
        }
        if (this._loadedAssets.garment) {
            this.assets.actualizeCache('garment', this._loadedAssets.garment);
        }

        this._clearContainer();
        const meshes = [];
        const materials = [];
        const extraMaps = { alphaMap: [], aoSecondMap: [] };

        assets.filter(Boolean).forEach((garmentAsset) => {
            // material
            if (!garmentAsset.object3d) {
                materials.push(garmentAsset);
                return;
            }

            garmentAsset.object3d.visible = !garmentAsset.hidden;

            if (garmentAsset.materials) {
                materials.push(...garmentAsset.materials);
            }

            this.container.add(garmentAsset.object3d);
            garmentAsset.object3d.scale.set(1, 1, 1);
            garmentAsset.object3d.position.y = -125;

            garmentAsset.settings = garmentAsset.settings || {};
            garmentAsset.settings.materialMap = garmentAsset.settings.materialMap || {};

            garmentAsset.object3d.traverse((object3d) => {
                object3d.scale.set(1, 1, 1);

                if (object3d instanceof THREE.Light) {
                    object3d.parent.remove(object3d);
                }

                if (!object3d.material) { return; }

                // save the real name
                object3d.userData.realName = object3d.userData.realName || object3d.name;

                // materialMap
                object3d.name = garmentAsset.settings.materialMap[object3d.name] || object3d.name;
                object3d.userData.asset = garmentAsset;

                meshes.push(object3d);
            });

            if (garmentAsset.hidden) return;

            for (let maps in extraMaps) {
                if (garmentAsset[maps] instanceof Array) {
                    garmentAsset[maps].forEach(map => { extraMaps[maps].push(map) });
                }
            }
        });

        for (let maps in extraMaps) {
            const canvases = {}, keyRegexp = /^\w+/;
            extraMaps[maps].forEach(map => {
                const key = map.for.match(keyRegexp)[0];
                const size = 256;
                const canvas = canvases[key] || createSquareCanvas(size); canvases[key] = canvas;
                const context = canvas.getContext('2d');
                context.globalCompositeOperation = 'multiply';
                context.drawImage(map.texture.image, 0, 0, size, size);
            });
            this.container.traverse(mesh => {
                delete mesh.userData[maps];
            });
            for(let key in canvases) {
                let texture = new THREE.Texture(canvases[key]); texture.needsUpdate = true;
                this.container.traverse(mesh => {
                    if (mesh.userData.realName && (mesh.userData.realName.indexOf(key) > -1)) {
                        mesh.userData[maps] = texture;
                    }
                });
            }
        }

        // FIXME find better way to hide the shirt buttons
        let jacketButtonAt = new THREE.Vector3 (0, -1e9, 0);

        [
            { buttonPrefix: 'buo', holeName: 'buttonholesjacket'},
            { buttonPrefix: 'btn', holeName: 'buttonholes'}
        ].forEach ( (pair, index) => {

        // if a button is found, remove it and clone into all button holes
        let button = this.container.children.find(object3d => (object3d.name.indexOf(pair.buttonPrefix) > -1));
        if (button) {
            this.container.remove(button);

            this.container.updateMatrixWorld(true);
            this.container.traverse(object3d => {
                if (object3d instanceof THREE.Mesh) {
                    let name = object3d.userData.realName || object3d.name;
                    if (name == pair.holeName) {
                        let result = this._placeButton(button, object3d, jacketButtonAt);
                        if (index == 0) {
                            jacketButtonAt = result.buttonholeAt;
                        } else {
                            object3d.visible = result.placed;
                        }
                    }
                }
            });
        }

        });

        materials.sort((material) => { return !material.objectApply ? -1 : 1; });

        this.onLoad(this.currentView, {meshes, assets, materials});

        this.meshes = meshes;
    }

    getMeshes(garment) {
        const {id} = this.getNormalizedGarment(garment);

        return this.meshes.filter(({userData: {asset}}) => {
            return asset.id === id;
        });
    }

    _clearContainer() {
        while (this.container.children.length) {
            this.container.remove(this.container.children[0]);
        }
    }

    _cloneWithProperMaterials(button) {
        // user data holds self-references that prevent cloning the button
        let userData = [], children = [], i = 0;
        button.traverse (function (object3d) {
            userData[i] = object3d.userData; object3d.userData = {};
            children[i] = object3d;
            i++;
        });

        let clone = button.clone();

        // restore button userData-s
        i = 0;
        button.traverse (function (object3d) {
            object3d.userData = userData[i];
            i++;
        });

        // assign material getters to clone children
        i = 0;
        clone.traverse (function (object3d) {
            if (object3d.material) {
                (function (originalObect3d) {
                    Object.defineProperty (object3d, 'material', {
                        get: function () {
                            return originalObect3d.material;
                        }
                    });
                }) (children[i]);
            }
            i++;
        });

        return clone;
    }

    _placeButton(button, buttonhole, above) {
        let placed = false;

        let box = new THREE.Box3();
        box.setFromObject(buttonhole);

        let buttonholeAt = new THREE.Vector3();
        box.getCenter(buttonholeAt);

        if (buttonholeAt.y > above.y) {
            placed = true;

            let clone = this._cloneWithProperMaterials(button);
            clone.name = '';

            this.container.worldToLocal(clone.position.copy(buttonholeAt));
            this.container.add(clone);

            // calculate offset and rotation from buttonhole normals
            let normal = new THREE.Vector3();
            for(let i = 0, n = buttonhole.geometry.attributes.normal.array; i < n.length; i += 3) {
                normal.x += n[i]; normal.y += n[i + 1]; normal.z += n[i + 2];
            }
            buttonhole.localToWorld(normal);

            let origin = new THREE.Vector3();
            buttonhole.localToWorld(origin);

            normal.sub(origin);

            clone.updateMatrixWorld(true);
            clone.localToWorld(origin.setScalar(0)).add(normal);
            clone.lookAt(origin);

            // debug
            // clone.add(new THREE.AxesHelper(5));
        }

        return { placed, buttonholeAt };
    }

    getNormalizedGarment(garment) {
        if (typeof garment === 'string') {
            return {id: garment};
        }

        return garment;
    }
}
