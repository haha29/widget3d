import * as dat from 'dat-gui';

import styles from './debug.css';

export default class MaterialTextureDebug {
    constructor(material, onChange) {
        const gui = new dat.GUI({autoPlace: false});
        document.body.appendChild(gui.domElement);
        const style = gui.domElement.style;
        style.right = 0;
        style.bottom = 0;
        style.position = 'fixed';

        const elementFiles = document.createElement('div');
        elementFiles.className = styles.root;
        document.body.append(elementFiles);

        const textures = [
            'map',
            'bumpMap',
            'specularMap',
            'rampMap'
        ];

        elementFiles.innerHTML = textures.reduce((result, textureName) => {
            return result + `
                <div class="${styles.title}">${textureName}</div>
                <div class="${styles.texture}">
                    <img id="${textureName}">
                    <input type="file" id="${textureName}input" name="texture" />
                </div>
            `;
        }, '');

        const elements = textures.reduce((result, textureName) => {
            result[textureName] = document.getElementById(textureName);

            return result;
        }, {});

        textures.forEach((textureName) => {
            document.getElementById(`${textureName}input`).addEventListener('change', event => this._handleChangeFile(event, textureName));
        });

        this._gui = gui;
        this._textures = textures;
        this._material = material;
        this._elements = elements;
        this._onChange = onChange;
        this._materials = [];
        this._guiNames = {};
    }

    addModelTextures(name, material) {
        const gui = this._gui;

        if (this._materials.includes(material)) { return; }

        this._materials.push(material);

        if (this._guiNames[name]) { return; }

        this._guiNames[name] = true;
        const guiFolder = gui.addFolder(name);
        guiFolder.add(material.defines, 'USE_MAP').name('diffuse').onChange((newValue) => {
            this._materials.forEach((mat) => {
                if (mat.name === name) {
                    mat.defines.USE_MAP = newValue;
                    mat.needsUpdate = true;
                    this._onChange();
                }
            });
        }).listen();
        [
            'aoMap',
            'normalMap',
            'bumpMap',
            'specularMap'
        ].forEach((textureName) => {
            if (material[textureName]) {
                guiFolder.add(material, textureName).onChange((newValue) => {
                    this._materials.forEach((mat) => {
                        if (mat.name === name) {
                            mat[textureName] = newValue;
                            mat.needsUpdate = true;
                            this._onChange();
                        }
                    });
                }).listen();
            }
        });
        // guiFolder.open();
    }

    updateTexture(materialKey, material) {
        if (!this._textures.includes(materialKey)) {
            return;
        }

        const tx = material.uniforms[materialKey].value;
        this._elements[materialKey].src = tx.image.src;
    }

    _handleChangeFile = (event, mapType) => {
        const file = event.target.files[0]; // FileList object
        const elements = this._elements;
        // const material = this._material;

        if (!file.type.match('image.*')) {
            return;
        }

        const reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (eventLoad) => {
            const src = eventLoad.target.result;
            elements[mapType].src = src;

            const newTextureImage = new Image();
            newTextureImage.src = src;

            this._materials.forEach((material) => {
                const tx = material.uniforms[mapType].value;
                tx.image = newTextureImage;
                tx.needsUpdate = true;
            });

            requestAnimationFrame(() => {
                this._onChange();
            });
        };

        // Read in the image file as a data URL.
        reader.readAsDataURL(file);
    }
}
