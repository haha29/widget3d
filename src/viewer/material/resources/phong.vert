#define PHONG

varying vec3 vViewPosition;

#ifndef FLAT_SHADED

	varying vec3 vNormal;

#endif

#include <common>
// include <uv_pars_vertex>
// #if defined( USE_AOMAP ) || defined( USE_NORMALMAP )

	varying vec2 vUv;
	uniform mat3 uvTransform;

// #endif


// include <uv2_pars_vertex>
#if defined( USE_MAP ) || defined( USE_BUMPMAP ) || defined ( USE_SPECULARMAP )

attribute vec2 uv2;
varying vec2 vUv2;

#endif
//

#include <displacementmap_pars_vertex>
#include <envmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>

void main() {
	// include <uv_vertex>
	// #if defined( USE_MAP ) || defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( USE_SPECULARMAP ) || defined( USE_ALPHAMAP ) || defined( USE_EMISSIVEMAP ) || defined( USE_ROUGHNESSMAP ) || defined( USE_METALNESSMAP )
		vUv = ( uvTransform * vec3( uv, 1 ) ).xy;
	// #endif

	//include <uv2_vertex>
    #if defined( USE_MAP ) || defined( USE_BUMPMAP ) || defined ( USE_SPECULARMAP )
        vUv2 = uv2;
    #endif
    //

	#include <color_vertex>

	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>

#ifndef FLAT_SHADED // Normal computed with derivatives when FLAT_SHADED

	vNormal = normalize( transformedNormal );

#endif

	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>

	vViewPosition = - mvPosition.xyz;

	#include <worldpos_vertex>
	#include <envmap_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>

    // #if defined( USE_AOMAP ) || defined( USE_NORMALMAP )
		#if defined( EXPORTER_BABYLON )
			vUv.y = - vUv.y;
		#elif defined( EXPORTER_BLENDER )
			vUv.y = 1. - vUv.y;
		#endif
    // #endif
}
