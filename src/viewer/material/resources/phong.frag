#define PHONG

uniform vec3 diffuse;
uniform vec3 emissive;
uniform vec3 specular;
uniform float shininess;
uniform float opacity;
uniform float diffuse2Gamma;
uniform float diffuse2Brightness;

uniform sampler2D rampMap;

#ifdef SEAM_MAP
	uniform vec3 seam;
	uniform float seamIntensity;
	uniform sampler2D seamMap;
#endif

#ifdef FONT_MAP
	uniform sampler2D fontMap;
	uniform float fontEdge;
	uniform float fontDistFrequency;
	uniform float fontDistAmplitude;
#endif

#include <common>
#include <packing>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
// include <uv_pars_fragment>
// #if defined( USE_AOMAP ) || defined( USE_NORMALMAP )

	varying vec2 vUv;

// #endif


// include <uv2_pars_fragment>
#if defined( USE_MAP ) || defined( USE_BUMPMAP ) || defined ( USE_SPECULARMAP )

uniform vec2 diffuseUVRepeatArray;
uniform vec2 otherUVRepeatArray;
varying vec2 vUv2;

#endif

//

#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <aomap_pars_fragment>
#ifdef USE_AOMAP_2
	uniform sampler2D aoMap2;
#endif
#include <lightmap_pars_fragment>
#include <emissivemap_pars_fragment>
#include <envmap_pars_fragment>
#include <gradientmap_pars_fragment>
#include <fog_pars_fragment>
#include <bsdfs>
#include <lights_pars_begin>
#include <lights_phong_pars_fragment>
#include <shadowmap_pars_fragment>

//include <bumpmap_pars_fragment>
#ifdef USE_BUMPMAP

	uniform sampler2D bumpMap;
	uniform float bumpScale;

	// Bump Mapping Unparametrized Surfaces on the GPU by Morten S. Mikkelsen
	// http://api.unrealengine.com/attachments/Engine/Rendering/LightingAndShadows/BumpMappingWithoutTangentSpace/mm_sfgrad_bump.pdf

	// Evaluate the derivative of the height w.r.t. screen-space using forward differencing (listing 2)

	vec2 dHdxy_fwd() {
		vec2 scaledUV = vUv2 * otherUVRepeatArray;

		vec2 dSTdx = dFdx( scaledUV );
		vec2 dSTdy = dFdy( scaledUV );

		float Hll = bumpScale * texture2D( bumpMap, scaledUV ).x;
		float dBx = bumpScale * texture2D( bumpMap, scaledUV + dSTdx ).x - Hll;
		float dBy = bumpScale * texture2D( bumpMap, scaledUV + dSTdy ).x - Hll;

		return vec2( dBx, dBy );

	}

	vec3 perturbNormalArb( vec3 surf_pos, vec3 surf_norm, vec2 dHdxy ) {

		// Workaround for Adreno 3XX dFd*( vec3 ) bug. See #9988

		vec3 vSigmaX = vec3( dFdx( surf_pos.x ), dFdx( surf_pos.y ), dFdx( surf_pos.z ) );
		vec3 vSigmaY = vec3( dFdy( surf_pos.x ), dFdy( surf_pos.y ), dFdy( surf_pos.z ) );
		vec3 vN = surf_norm;		// normalized

		vec3 R1 = cross( vSigmaY, vN );
		vec3 R2 = cross( vN, vSigmaX );

		float fDet = dot( vSigmaX, R1 );

		fDet *= ( float( gl_FrontFacing ) * 2.0 - 1.0 );

		vec3 vGrad = sign( fDet ) * ( dHdxy.x * R1 + dHdxy.y * R2 );
		return normalize( abs( fDet ) * surf_norm - vGrad );

	}

#endif

//


#include <normalmap_pars_fragment>
#include <specularmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>

void main() {

	#include <clipping_planes_fragment>

	vec4 diffuseColor = vec4( diffuse, opacity );
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	vec3 totalEmissiveRadiance = emissive;

	#include <logdepthbuf_fragment>

	//include <map_fragment>
	#ifdef USE_MAP

	vec4 texelColor = texture2D( map, vUv2 * diffuseUVRepeatArray );

	texelColor = mapTexelToLinear( texelColor );
	diffuseColor *= texelColor;

	#endif

	#ifdef SEAM_MAP
		float seamFactor = texture2D(seamMap, vUv).x;
		diffuseColor.rgb = mix(diffuseColor.rgb, seam, seamFactor * seamIntensity);
	#endif


	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>

	//include <specularmap_fragment>
	float specularStrength;

	#ifdef USE_SPECULARMAP

		vec4 texelSpecular = texture2D( specularMap, vUv2 * otherUVRepeatArray );
		specularStrength = texelSpecular.r;

	#else

		specularStrength = 1.0;

	#endif
	//

	#include <normal_fragment_begin>

	// include <normal_fragment_maps>
	#ifdef USE_NORMALMAP

		#ifdef OBJECTSPACE_NORMALMAP

			normal = texture2D( normalMap, vUv ).xyz * 2.0 - 1.0; // overrides both flatShading and attribute normals

			#ifdef FLIP_SIDED

				normal = - normal;

			#endif

			#ifdef DOUBLE_SIDED

				normal = normal * ( float( gl_FrontFacing ) * 2.0 - 1.0 );

			#endif

			normal = normalize( normalMatrix * normal );

		#else // tangent-space normal map

			// normal = perturbNormal2Arb( -vViewPosition, normal );

			#if defined( USE_BUMPMAP )

				vec3 normalNormal = perturbNormal2Arb( -vViewPosition, normal );
				vec3 bumpNormal = perturbNormalArb( -vViewPosition, normal, dHdxy_fwd() );

				normal = normalize(normalNormal + bumpNormal);

			#else

				normal = perturbNormal2Arb( -vViewPosition, normal );

			#endif
		#endif

	#elif defined( USE_BUMPMAP )

		normal = perturbNormalArb( -vViewPosition, normal, dHdxy_fwd() );

	#endif

	//
	// Fall Off

	float cameraAngle = 1. - dot(normal, normalize(vViewPosition));
	float falloffFactor = texture2D(rampMap, vec2(cameraAngle, 0.)).r;
	vec3 diffuse2Color = pow(diffuseColor.rgb, vec3(1.0 / diffuse2Gamma)) + diffuse2Brightness;
	diffuseColor.rgb = mix(diffuseColor.rgb, diffuse2Color, falloffFactor);

	//


	#include <emissivemap_fragment>

	// accumulation
	#include <lights_phong_fragment>
	#include <lights_fragment_begin>
	#include <lights_fragment_maps>
	#include <lights_fragment_end>

	// modulation
	// include <aomap_fragment>
	#ifdef USE_AOMAP

		// reads channel R, compatible with a combined OcclusionRoughnessMetallic (RGB) texture
		float ambientOcclusion = texture2D( aoMap, vUv ).r;
		#ifdef USE_AOMAP_2
			ambientOcclusion = ambientOcclusion * texture2D( aoMap2, vUv ).r;
		#endif
		ambientOcclusion = ( ambientOcclusion - 1.0 ) * aoMapIntensity + 1.0;

		reflectedLight.indirectDiffuse *= ambientOcclusion;

		#if defined( USE_ENVMAP ) && defined( PHYSICAL )

			float dotNV = saturate( dot( geometry.normal, geometry.viewDir ) );

			reflectedLight.indirectSpecular *= computeSpecularOcclusion( dotNV, ambientOcclusion, material.specularRoughness );

		#endif

	#endif
	//end aomap_fragment

	vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;

	#include <envmap_fragment>

	#ifdef FONT_MAP
		float dist = sin(vUv.y * fontDistFrequency) * fontDistAmplitude / 1000.;
		vec4 fontTexel = texture2D(fontMap, vec2(vUv.x + dist, vUv.y));
		diffuseColor.a = fontTexel.a;

		#ifdef USE_AOMAP
			vec4 aoTexel = texture2D(aoMap, vUv);
			diffuseColor.a = mix(smoothstep(fontEdge / 100., 1., aoTexel.r) * diffuseColor.a, diffuseColor.a, 1. - fontTexel.r);
		#endif
	#endif

	gl_FragColor = vec4( outgoingLight, diffuseColor.a );


	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>


}
