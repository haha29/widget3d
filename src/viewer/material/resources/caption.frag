precision highp float;

uniform vec3 color;
uniform sampler2D map;

varying vec2 vUV;

void main() {
    vec4 texel = texture2D(map, vUV);

    gl_FragColor = vec4(texel.rgb, texel.a);
}
