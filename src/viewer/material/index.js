/* eslint-disable class-methods-use-this */
import * as THREE from 'three';

import get from 'lodash/get';
import template from 'lodash/template';
import templateSettings from 'lodash/templateSettings';
import dispose from '../../utils/dispose';

import TexturesGUI from './debug';

import fragShaderTemplate from './resources/phong.frag';
import vertShaderTemplate from './resources/phong.vert';

// import fragCaptionShaderTemplate from './resources/caption.frag';
// import vertCaptionShaderTemplate from './resources/caption.vert';

const bakTemplate = templateSettings.interpolate;
templateSettings.interpolate = /#include <([\s\S]+?)>/g;

const fragShader = /* '#extension GL_OES_standard_derivatives : enable \n' +  */template(fragShaderTemplate)(THREE.ShaderChunk);
const vertShader = template(vertShaderTemplate)(THREE.ShaderChunk);

// const captionFragShader = template(fragCaptionShaderTemplate)(THREE.ShaderChunk);
// const captionVertShader = template(vertCaptionShaderTemplate)(THREE.ShaderChunk);

templateSettings.interpolate = bakTemplate;

const directProperties = new Set(['side']);

export default class Material {
    constructor({onChange, isDebug, anisotropy}) {
        this._onChange = onChange;
        this._anisotropy = anisotropy;

        this._material = this._createMaterial();
        this._materials = [];

        this._fontCache = {};
        this._textCache = {};

        if (isDebug) {
            /* eslint-disable import/no-unresolved, global-require */
            require.ensure([], () => {
                this._gui = new TexturesGUI(this._material, onChange);
            }, 'debug');
            /* eslint-enable */
        }
    }

    dispose() {
        dispose(this);
    }

    _createMaterial() {
        const material = new THREE.ShaderMaterial({
            uniforms: THREE.UniformsUtils.merge([
                {diffuse: {}},
                {seam: {}},
                {seamIntensity: {value: 0.99}},
                {diffuse2Gamma: {value: 1.29}},
                {diffuse2Brightness: {value: 0.02}},
                {emissive: {}},
                {specular: {}},
                {shininess: {}},
                {diffuseUVRepeatArray: {value: new THREE.Vector2(1, 1)}},
                {otherUVRepeatArray: {value: new THREE.Vector2(1, 1)}},
                {aoMap2: {}},
                {aoMapIntensity: {}},
                {fontEdge: {value: 24}},
                {fontDistFrequency: {value: 24}},
                {fontDistAmplitude: {value: 3}},
                {bumpScale: {value: 0.99}},
                {normalScale: {}},
                THREE.ShaderLib.phong.uniforms,
                {rampMap: {}},
                {seamMap: {}},
                {fontMap: {}},
            ]),
            vertexShader: vertShader,
            fragmentShader: fragShader,
            lights: true,
            defines: {
                USE_MAP: false
                // USE_NORMALMAP: true,
                // USE_AOMAP: true,
                // USE_BUMPMAP: true,
                // USE_SPECULARMAP: true
            }
        });

        // material.map = true;
        // material.aoMap = true;
        // material.bumpMap = true;
        // material.normalMap = true;
        // material.specularMap = true;

        material.uniforms.bumpScale.value = 0.99;
        material.uniforms.aoMapIntensity.value = 0.99;

        return material;
    }

    _applyMaterial(material, settings, assets) {
        let isUpdated = false;
        Object.entries(settings).forEach(([materialKey, materialValue]) => {
            if (typeof materialValue === 'string' && materialValue.startsWith('$')) {
                const tx = get(assets, materialValue.replace('$', ''));

                if (!tx) { console.error('misconfigured texture', materialValue, assets.id); return; }

                tx.anisotropy = this._anisotropy;

                if (material instanceof THREE.ShaderMaterial) {
                    if (material.uniforms[materialKey].value !== tx) {
                        material.uniforms[materialKey].value = tx;

                        if (materialKey !== 'map') {
                            material[materialKey] = true;
                        }

                        tx.needsUpdate = true;
                        isUpdated = true;
                    }
                } else if (material[materialKey] !== tx) {
                    material[materialKey] = tx;

                    tx.needsUpdate = true;
                    isUpdated = true;
                }
            } else if (typeof materialValue === 'string' && materialValue.startsWith('#')) {
                const color = new THREE.Color().setStyle(materialValue);
                if (material instanceof THREE.ShaderMaterial) {
                    material.uniforms[materialKey].value = color;
                } else {
                    material[materialKey] = color;
                }
            } else if (Array.isArray(materialValue)) {
                if (material instanceof THREE.ShaderMaterial) {
                    material.uniforms[materialKey].value.fromArray(materialValue);
                } else {
                    material[materialKey].fromArray(materialValue);
                }
            } else {
                // eslint-disable-next-line no-lonely-if
                if (material instanceof THREE.ShaderMaterial && !directProperties.has(materialKey)) {
                    material.uniforms[materialKey].value = materialValue;
                } else {
                    material[materialKey] = materialValue;
                }
            }

            if (this._gui && material instanceof THREE.ShaderMaterial) {
                this._gui.updateTexture(materialKey, material);
            }
        });

        if (material.uniforms) {
            if (material.uniforms.map.value) {
                material.defines.USE_MAP = true;
            }
            if (material.uniforms.seamMap.value && material.uniforms.seam.value) {
                material.defines.SEAM_MAP = true;
            }
            ['aoMap', 'bumpMap', 'normalMap', 'specularMap'].forEach((textureName) => {
                if (material.uniforms[textureName].value) {
                    material[textureName] = true;
                }
            });
        }

        if (this._gui && material instanceof THREE.ShaderMaterial) {
            this._gui.addModelTextures(material.name, material);
        }

        if (isUpdated) {
            material.needsUpdate = true;
        }
    }

    _giveName(object3d, postfix) {
        object3d.material.name = object3d.userData.originalName;
        object3d.name = `${object3d.userData.originalName} (${postfix})`;
    }

    getTextTexture(text, size = 50, file, style = 'normal') {
        if (!text) return new THREE.DataTexture( new Uint8Array( 4 ), 1, 1, THREE.RGBAFormat );

        const key = JSON.stringify(arguments);
        if (this._textCache[key]) return this._textCache[key];

        const canvas = document.createElement('canvas');
        canvas.width = 256;
        canvas.height = 256;

        if (this._gui) {
            let canvasGUIElement = document.querySelector('#canvasgui');

            if (!canvasGUIElement) {
                canvasGUIElement = document.createElement('div');
                canvasGUIElement.id = 'canvasgui';
                canvasGUIElement.style.position = 'absolute';
                canvasGUIElement.style.left = '0';
                canvasGUIElement.style.bottom = '0';
                canvasGUIElement.style.border = '1px solid black';
                canvas.style.width = '128px';
                canvas.style.height = '128px';

                document.body.appendChild(canvasGUIElement);
            }

            canvasGUIElement.innerHTML = '';
            canvasGUIElement.appendChild(canvas);
        }

        const texture = new THREE.Texture(canvas);

        const font = btoa(file).replace(/\W/g, '');
        const renderText = (fontFace) => {
            if (fontFace) {
                document.fonts.add(fontFace);
                this._fontCache[font] = true;
            }
            const ctx = canvas.getContext('2d');
            ctx.font = `${style} ${size}px ${font}`;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.strokeStyle = 'red';
            ctx.lineWidth = 3;
            ctx.fillText(text, 128, 256);
            ctx.strokeText(text, 128, 256);
            texture.needsUpdate = true;
        };

        if (this._fontCache[font]) {
            renderText();
        } else {
            const fontFace = new FontFace(font, 'url(' + file + ')');
            fontFace.load().then(renderText);
        }

        this._textCache[key] = texture;
        return texture;
    }

    setModel(assets) {
        const wrapper3d = assets.object3d;
        const settings = assets.settings.materials || (assets.text && assets.text.settings.materials);

        if(!(wrapper3d.visible = !!settings)) {
            console.warn('[clothes-widget-3d] no material settings for ' + assets.id);
            return;
        }

        const material = this._material.clone();

        if (wrapper3d.userData.exporter === 'babylon') {
            material.defines.EXPORTER_BABYLON = true;
        } else if (wrapper3d.userData.exporter === 'blender') {
            material.defines.EXPORTER_BLENDER = true;
        }

        let fontMap;

        if (assets.text && typeof assets.text.value === 'string') {
            fontMap = this.getTextTexture(assets.text.value, assets.text.size, assets.text.path + assets.text.file, assets.text.style);
        }

        wrapper3d.traverse((object3d) => {
            if (!(object3d instanceof THREE.Mesh)) { return; }

            object3d.userData.originalName = object3d.userData.originalName || object3d.name;
            let object3dSettings = settings[object3d.userData.originalName];

            // Super hardcode hack, set cotton as default material
            if (!object3dSettings) {
                const DEFAULT_MATERIAL = 'cotton';
                object3dSettings = settings[DEFAULT_MATERIAL];
                if (!object3dSettings) console.warn('[clothes-widget-3d] no material settings for ' + assets.id + ', mesh name: ' + object3d.userData.originalName);
                object3d.name = DEFAULT_MATERIAL;
                object3d.userData.originalName = DEFAULT_MATERIAL;
            }

            const parts = wrapper3d.name.split('/');
            const name = parts[parts.length - 1];
            if (!object3dSettings) {
                object3d.material = new THREE.MeshPhongMaterial();
                this._giveName(object3d, name);
                this._materials.push(object3d.material);
                return;
            }

            const {type, ...materialSettings} = object3dSettings;

            if (type) {
                object3d.material = new THREE[type]();
            } else if (object3d.material.type !== 'ShaderMaterial') {
                object3d.material = material.clone();
            }

            this._giveName(object3d, name);
            this._materials.push(object3d.material);
            object3d.material.userData.objectId = object3d.userData.asset.id;

            if (!object3d.geometry.attributes.uv2) {
                object3d.geometry.attributes.uv2 = object3d.geometry.attributes.uv;
            }

            this._applyMaterial(object3d.material, materialSettings, assets);

            if (assets.text) {
                if (assets.text.color) {
                    object3d.material.uniforms.diffuse.value.setStyle(assets.text.color);
                }

                if (typeof assets.text.value === 'string' && fontMap) {
                    // If wasn't set before need shader recompile
                    if (!object3d.material.uniforms.fontMap.value) {
                        object3d.material.needsUpdate = true;
                        object3d.material.defines.FONT_MAP = true;
                        object3d.material.transparent = true;
                    }

                    object3d.material.uniforms.fontMap.value = fontMap;
                }
            }

            if (object3d.material.uniforms) {
                object3d.material.uniforms.alphaMap.value = object3d.userData.alphaMap;
                if (object3d.userData.alphaMap) {
                    object3d.material.defines.USE_ALPHAMAP = true;
                    object3d.material.defines.ALPHATEST = 0.5;
                } else {
                    delete object3d.material.defines.USE_ALPHAMAP;
                    delete object3d.material.defines.ALPHATEST;
                }

                object3d.material.uniforms.aoMap2.value = object3d.userData.aoSecondMap;
                if (object3d.userData.aoSecondMap) {
                    object3d.material.defines.USE_AOMAP_2 = true;
                } else {
                    delete object3d.material.defines.USE_AOMAP_2;
                }
            } else {
                object3d.material.alphaMap = object3d.userData.alphaMap;
                object3d.material.alphaTest = object3d.userData.alphaMap ? 0.5 : 0;
            }
        });

        // Test sphere
        // const geometry = new THREE.SphereBufferGeometry(15, 32, 32);
        // const sphere = new THREE.Mesh(geometry, material);
        // sphere.position.set(0, 170, 0);
        // wrapper3d.add(sphere);
    }

    setMaterial(assets) {
        if (!assets || !assets.settings || !assets.settings.materials) {
            return;
        }
        const settings = assets.settings.materials;

        this._materials.forEach((material) => {
            const materialSettings = settings[material.name];

            if (!materialSettings) { return; }

            if (assets.objectApply && material.userData.objectId !== assets.objectApply) {
                return;
            }

            this._applyMaterial(material, materialSettings, assets);
        });
    }
}

// function logImage(tx) {
//     console.log('%c', `
//         font-size: 1px;
//         line-height: 333px;
//         padding: 166.5px 250px;
//         background-size: 500px 333px;
//         background: url('${tx.value.image.toDataURL()}');`
//     );
// }
