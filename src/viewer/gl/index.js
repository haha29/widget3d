import * as THREE from 'three';
import dispose from '../../utils/dispose';

const devicePixelRatio = window.devicePixelRatio || 1;
const defaultConfig = {
    clearColor: 0x000000,
    antialias: true,
    alpha: true,
    pixelRatio: devicePixelRatio
};

export default class Gl {
    constructor({config = {}, scene, camera} = {}) {
        config = Object.assign({}, defaultConfig, config); // eslint-disable-line no-param-reassign
        const {antialias, alpha, clearColor, pixelRatio} = config;

        const renderer = new THREE.WebGLRenderer({antialias, alpha});
        renderer.setClearColor(clearColor, +!alpha);
        renderer.setPixelRatio(Math.min(pixelRatio, devicePixelRatio));
        const domElement = renderer.domElement;

        Object.assign(this, {
            config,
            renderer,
            scene,
            camera,
            _mainLoop: f => f,
            _additionalLoop: f => f,
            _constLoop: f => f,
            _isRunning: false,
            domElement
        });

        this._loop = this._loop.bind(this);
        this._loop2 = this._loop2.bind(this);
    }

    updateSize(w, h) {
        this.renderer.setSize(w, h);
    }

    render() {
        this.renderer.render(this.scene, this.camera);
    }

    setMainLoop(fn) {
        this._mainLoop = fn;
    }

    setConstLoop(fn) {
        this._constLoop = fn;
    }

    addAdditionalLoop(fn) {
        this._additionalLoop = fn;
    }

    run() {
        if (this._isRunning) { return; }
        this._isRunning = true;

        window.requestAnimationFrame(this._loop);
    }

    stop() {
        this._isRunning = false;
    }

    stopAll() {
        window.cancelAnimationFrame(this._loop);
        window.cancelAnimationFrame(this._loop2);
    }

    runOnce() {
        this._mainLoop();
        this._additionalLoop();
    }

    runConstOnce() {
        this._constLoop();
    }

    runConstLoop() {
        window.requestAnimationFrame(this._loop2);
    }

    dispose() {
        this.stopAll();
        if (this.renderer) {
            this.renderer.dispose();
        }
        dispose(this);
    }

    _loop() {
        this._mainLoop();
        this._additionalLoop();

        if (this._isRunning) {
            window.requestAnimationFrame(this._loop);
        }
    }

    _loop2() {
        if (this._loop2) {
            this._constLoop();
            window.requestAnimationFrame(this._loop2);
        }
    }
}

