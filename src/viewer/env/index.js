import * as THREE from 'three';

// const tempVector = new THREE.Vector3();

export default class Env {
    constructor({config}) {
        const scene = new THREE.Scene();
        scene.name = 'Scene';

        const wrapper = new THREE.Group();
        wrapper.name = 'Wrapper';
        wrapper.position.y = 35;
        wrapper.scale.set(0.98, 0.98, 0.98);
        scene.add(wrapper);

        const lights = new THREE.Group();
        lights.name = 'Lights';
        // lights.position.set(0, -125, 0);

        const ambientLight = new THREE.AmbientLight(0xffffff, 1.8);
        ambientLight.name = 'ambientLight';

        const directionalLightLeft = new THREE.DirectionalLight(0xffffff, 0.05);
        directionalLightLeft.name = 'directionalLightLeft';
        directionalLightLeft.position.set(-279, 182, 81);
        directionalLightLeft.target.position.set(-23, 147, -97);

        const directionalLightRight = new THREE.DirectionalLight(0xffffff, 0.4);
        directionalLightRight.name = 'directionalLightRight';
        directionalLightRight.position.set(155, 246, 193);
        directionalLightRight.target.position.set(-17, 143, 50);

        wrapper.add(lights);
        lights.add(ambientLight);
        lights.add(directionalLightRight);
        lights.add(directionalLightLeft);

        this.config = config;
        this.scene = scene;
        this.camera = null;
        this.lights = lights;
        this.wrapper = wrapper;

        this._frustum = new THREE.Frustum();
        this._matrix = new THREE.Matrix4();
    }

    dispose() {
        this.scene.traverse((child) => {
            if (!(child instanceof THREE.Mesh)) {
                return;
            }

            child.geometry.dispose();
            this._disposeMaterial(child.material);
        });

        this.scene.children.forEach((child) => {
            this.scene.remove(child);
        });
    }

    _disposeMaterial(material) {
        if (!material) {
            return;
        }

        if (material instanceof THREE.ShaderMaterial) {
            Object.keys(material.uniforms).forEach((uniformName) => {
                const uniform = material.uniforms[uniformName];

                if (uniform.value instanceof THREE.Texture) {
                    uniform.value.dispose();
                }
            });
        } else {
            Object.keys(material).forEach((key) => {
                if (material[key] instanceof THREE.Texture) {
                    material[key].dispose();
                }
            });
        }

        material.dispose();
    }

    setCamera(camera) {
        this.camera = camera;

        this.scene.add(camera);
    }

    // animate() {
        // this.camera.getWorldDirection(tempVector);

        // // Rotate lights with camera
        // this.lights.rotation.y = Math.atan2(tempVector.x, tempVector.z) + Math.PI;
    // }

    isInFrustum(object) {
        if (!object) { return false; }

        const camera = this.camera;

        camera.updateMatrix();
        camera.updateMatrixWorld();
        camera.matrixWorldInverse.getInverse(camera.matrixWorld);

        object.updateMatrix();
        object.updateMatrixWorld();

        this._matrix.multiplyMatrices(camera.projectionMatrix, camera.matrixWorldInverse);
        this._frustum.setFromMatrix(this._matrix);

        return this._frustum.intersectsObject(object);
    }
}
