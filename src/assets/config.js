import GarmentType from './types/garment';
import ModelType from './types/model';
import TextureType from './types/texture';

export default function(assets) {
    const {assetsPath = '', apiPath = '', dracoPath = 'https://www.gstatic.com/draco/versioned/decoders/1.4.1/', salonId = '1', garmentIds = {}} = assets.config;
    const types = assets._types;

    types.garment = new GarmentType({basePath: apiPath, salonId, garmentIds});
    types.model = new ModelType({basePath: assetsPath, dracoPath});
    types.texture = new TextureType({basePath: assetsPath});
}
