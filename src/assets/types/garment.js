import url from 'url';
import template from 'lodash/template';
import httpLoader from '../helpers/http';
import * as cache from '../helpers/cache';

export default class GarmentType {
    constructor({basePath, salonId, garmentIds}) {
        const urlPrefix = url.resolve(basePath, 'garments/configUrl3d');
        this._template = `${urlPrefix}/<%= id %>?salonId=${salonId}&garmentId=<%= garmentId %>`;
        this._cache = {};
        this._garmentIds = garmentIds;
    }

    getById(id) {
        if (this._cache[id]) {
            return Promise.resolve(this._cache[id]);
        }

        const idParts = id.split(':');

        const garmentId = idParts[1] || this._garmentIds[id.substr(0, 3)] || 'shirt';

        const src = template(this._template)({id: idParts[0], garmentId});

        return httpLoader.load(src, JSON.parse).then((json) => {
            this._cache[id] = json;

            return json;
        });
    }

    actualizeCache = cache.actualize
}
