import * as THREE from 'three';
import 'three/examples/js/loaders/LoaderSupport';
import 'three/examples/js/loaders/DRACOLoader';
import 'three/examples/js/loaders/GLTFLoader';
import template from 'lodash/template';
import * as cache from '../helpers/cache';
import * as pako from 'pako';

const loaderGZ = new THREE.FileLoader();
loaderGZ.setResponseType('arraybuffer');

const loaderGLTF = new THREE.GLTFLoader();
const loaderDraco = new THREE.DRACOLoader();

// we want to cache the content of .bin.gz file, but if 3js
// cache is enabled, any other 3js loadig process running in
// parallel elsewhere is affected. so instead we will enable
// FileLoader to read the cache even if it is disabled
THREE.Cache.get = function(key) { return this.files[key]; };

// const loader = new THREE.OBJLoader2();
// loader.meshBuilder.setLogging(false);
// loader.logging.enabled = false;

function getExporter(asset) {
    const generator = (asset.generator || '').toLowerCase();

    if (generator.includes('babylon')) {
        return 'babylon';
    }

    if (generator.includes('blender')) {
        return 'blender';
    }

    return 'unknown';
}

export default class ModelType {
    constructor({basePath, dracoPath}) {
        this._template = `${basePath}<%= id %>`;

        this._cache = {};

        if(!loaderGLTF.dracoLoader && dracoPath) {
            THREE.DRACOLoader.setDecoderPath(dracoPath);
            loaderGLTF.setDRACOLoader(loaderDraco);
        }
    }

    getById(id) {
        if (this._cache[id]) {
            return Promise.resolve(this._cache[id]);
        }

        const src = template(this._template)({id});

        return new Promise((resolve, reject) => {
            const loadGLTF = () => {
                loaderGLTF.load(src, ({scene, asset}) => {
                    const wrapper = new THREE.Group();
                    wrapper.name = id;

                    [...scene.children].forEach(child => wrapper.add(child));

                    wrapper.userData.exporter = getExporter(asset);

                    this._cache[id] = wrapper;

                    resolve(wrapper);
                }, undefined, reject);
            };

            // attempt to load .bin.gz file first
            loaderGZ.load(src.replace(/gltf$/, 'bin.gz'), (response) => {
                var bin;
                try {
                    bin = pako.ungzip(new Uint8Array(response));
                } catch (notGZip) {}
                if (bin) {
                    THREE.Cache.files[loaderGZ.manager.resolveURL(src.replace(/gltf$/, 'bin'))] = bin.buffer;
                }
                loadGLTF();
            }, undefined, loadGLTF);
        });
    }

    actualizeCache = cache.actualize
}
