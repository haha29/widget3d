import * as THREE from 'three';
import template from 'lodash/template';
import * as cache from '../helpers/cache';

const loader = new THREE.TextureLoader();

export default class TextureType {
    constructor({basePath}) {
        this._template = `${basePath}<%= id %>`;

        this._cache = {};
    }

    getById(id) {
        if (this._cache[id]) {
            return Promise.resolve(this._cache[id]);
        }

        let repeat = false;

        if (typeof id === 'object') {
            repeat = id.repeat;
            id = id.src; // eslint-disable-line
        }

        return new Promise((resolve, reject) => {
            const finalSrc = template(this._template)({id});

            loader.load(finalSrc,
                (tx) => {
                    if (repeat) {
                        tx.wrapS = THREE.RepeatWrapping;
                        tx.wrapT = THREE.RepeatWrapping;
                        tx.needsUpdate = true;
                    }

                    this._cache[id] = tx;

                    resolve(tx);
                },
                undefined, // progress callback
                (err) => {
                    console.warn('[clothes-widget-3d] could not load ' + finalSrc);

                    // fall back to white pixel png instead

                    let image = new Image();
                    image.onload = () => {
                        let texture = new THREE.Texture (image);
                        texture.needsUpdate = true;
                        resolve(texture);
                    }

                    image.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAC0lEQVQYV2P4DwQACfsD/SvVCEUAAAAASUVORK5CYII=';

                    // reject(err);
                }
            );
        });
    }

    actualizeCache = cache.actualize
}
