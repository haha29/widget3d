import createLodash from 'lodash/create';

const showErr = err => console.error(err); // eslint-disable-line no-console

const proto = {
    _getByType(type) {
        const assets = this._types[type];

        if (!assets) { return showErr(`Asset type ${type} not found`); }

        return assets;
    }
};

Object.keys(require('./interfaces/loader')).forEach((method) => {
    proto[method] = function(type, ...args) {
        const assets = this._getByType(type);

        return assets[method](...args);
    };
});

export default function Assets({config = {}} = {}) {
    const instance = createLodash(proto, {
        config,
        _types: {}
    });

    require('./config').default(instance); // eslint-disable-line global-require

    return instance;
}
