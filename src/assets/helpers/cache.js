export function actualize(ids) {
    const idsMap = ids.reduce((result, id) => {
        result[id] = true;

        return result;
    }, {});

    Object.keys(this._cache).forEach((cachedId) => {
        if (!idsMap[cachedId]) {
            this._cache[cachedId] = undefined;
        }
    });
}

