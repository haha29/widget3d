exports.load = function(src, handler) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function() {
            if (xhr.readyState !== 4) {
                return;
            }

            if ([200, 304].indexOf(xhr.status) === -1) {
                reject(new Error(`Server responded with a status of ${xhr.status}`));
            } else {
                resolve(handler ? handler(xhr.responseText) : xhr.responseText);
            }
        };

        xhr.open('GET', src, true);
        xhr.send(null);
    });
};
