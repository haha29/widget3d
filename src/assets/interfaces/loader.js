function error() {
    console.error('Not implemented'); // eslint-disable-line no-console
}

const methods = ['getList', 'getById', 'getFirst', 'actualizeCache'];

module.exports = methods.reduce((reducing, method) => {
    reducing[method] = error;
    return reducing;
}, {});
